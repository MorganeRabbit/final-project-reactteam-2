import React, {useState} from 'react';
import "./connexion.css";
import "../../Assets/Styles/styles.css";
import "../../App";
import AuthService from "../../Services/auth.service";


const Connexion = (props) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [message, setMessage] = useState("");

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const handleLogin = (e) => {
        e.preventDefault();
        setMessage("");
        AuthService.login(username, password).then(
            () => {
                window.location.href = "/formation-creation";
                //props.history.push("/formation-creation");
                //window.location.reload();
            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
                setMessage(resMessage);
            }
        );
    }


    return (
        <div className="background-grey">
            <div className="container box background-white">
                <div className="row">
                    <div className="col-12 d-flex justify-content-center">
                        <h1 className="title-blue title-connection-page py-5">Editeur de Formation</h1>
                    </div>
                    <div className="col-12 d-flex justify-content-center">
                        <p className="title-black text-connection-page py-5">Connexion</p>
                    </div>


                    <form onSubmit={handleLogin} className="col-10 offset-1 pb-5 mb-5">
                        <input className="form-control inputs-zones mb-3"
                               name="username"
                               value={username}
                               onChange={onChangeUsername}
                               required={true}
                               placeholder="Login..."
                               type="text"
                        />
                        <input className="form-control inputs-zones"
                               type="password"
                               placeholder="Mot de passe..."
                               name="password"
                               value={password}
                               onChange={onChangePassword}
                               required={true}
                        />
                        <div className="py-4"> </div>


                        <div className="col-12 d-flex justify-content-center mt-5 pt-5">
                            <button type="submit" className="button-blue-big text-button valider">Valider</button>
                        </div>
                        {message && (
                            <div className="form-group">
                                <div className="alert alert-danger mt-3" role="alert">
                                    {message}
                                </div>
                            </div>
                        )}

                    </form>
                </div>
            </div>
        </div>
    )
}

export default Connexion;
