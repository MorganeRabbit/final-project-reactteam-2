import React, {useState} from "react";
import "../../../Assets/Styles/styles.css";
import "../lesson-creation.css";
import ButtonSave from "../../Buttons/button-save-component";
import ButtonLogOut from "../../Buttons/button-logout-component";
import ModifyIcon from "../../../Assets/Icons/Vector.svg"


const Header = () => {

    const [title, setTitle] = useState("");
    const [currentTitle, setCurrentTitle] = useState("");


    const onChangeTitle = (e) => {
        const title = e.target.value;
        setTitle(title);
    };

    const handleSave = () => {
    }

    return (
        <div className="container-fluid background-white">
            <div className="row py-4 mx-3 align-items-center">

                {currentTitle ? (
                    <>
                        <h1 className="text-header-black mx-2">Contenus</h1>
                        <h1 className="text-header-blue mx-2"> / {title} </h1>
                        <button className="modifyIcon">
                            <img src={ModifyIcon} alt="ImageFailed"/>
                        </button>

                    </>


                ) : (
                    <>
                        <h1 className="text-header-black mx-2">Editeur de formation</h1>
                        <input className="form-control inputs-zones name-header-input col-3 mx-2"
                               name="title"
                               type="text"
                               placeholder="Nom de la formation ..."
                               value={title}
                               onChange={onChangeTitle}

                        />
                        <ButtonSave onClick={handleSave}/>
                    </>
                )}


                <button className="button-blue-big col-1 offset-3">
                    <div className="text-button">Publier</div>
                </button>
                <ButtonLogOut/>
            </div>
        </div>
    )
}

export default Header;
