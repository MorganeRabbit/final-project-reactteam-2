import http from "../http-common";


const getAll = () => {
    return http.get("/chapter");
};

const get = id => {
    return http.get(`/chapter/${id}`);
};

const create = data => {
    return http.post("/chapter/", data);
};

const update = (id, data) => {
    return http.put(`/chapter/${id}`, data);
};

const remove = id => {
    return http.delete(`/chapter/${id}`);
};

const findByTitle = title => {
    return http.get(`/chapter?title=${title}`);
};

export default {
    getAll,
    create,
    update,
    get,
    remove,
    findByTitle
};
