import http from "../http-common";


const getAll = () => {
    return http.get("/formation");
};

const get = id => {
    return http.get(`/formation/${id}`);
};

const create = data => {
    return http.post("/formation/", data);
};

const update = (id, data) => {
    return http.put(`/formation/${id}`, data);
};

const remove = id => {
    return http.delete(`/formation/${id}`);
};

const findByTitle = title => {
    return http.get(`/formation?title=${title}`);
};

export default {
    getAll,
    create,
    update,
    get,
    remove,
    findByTitle
};
