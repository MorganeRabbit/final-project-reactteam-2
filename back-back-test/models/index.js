const mongoose = require('mongoose');

const db = {};

db.mongoose = mongoose;

db.user = require("./user-model");
db.lesson = require ("./lesson-model");

module.exports = db;
