const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

let corsOptions = {
    origin: "http://localhost:3000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome, voyageur." });
});

// Router
const verifyToken = require("./middlewares/authJwt");

const AuthRouter = require('./Router/auth-routes');
app.use(AuthRouter.prefix, AuthRouter.router);

const LessonRouter = require("./Router/lessonsRouter");
app.use(LessonRouter.prefix, LessonRouter.router);

//require('./Router/user.Router')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log('Server is running on port: http://localhost:' + PORT);
});


module.exports = app;
