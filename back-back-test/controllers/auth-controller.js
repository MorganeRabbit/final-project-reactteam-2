const config = require("../config/auth-config");
const db = require("../models");
const User = db.user;

let jwt = require("jsonwebtoken");
let bcrypt = require("bcryptjs");


exports.create = (req,res) => {
    const user = new User (req.body)
    user.save().then(res.send("utilisateur enregistrer")).catch((err) => res.send (err))

}

exports.signin = (req, res) => {
    User.findOne({
        username: req.body.username
    })
        .exec((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            if (!user) {
                return res.status(404).send({ message: "User Not found." });
            }

            let passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            let token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 3600 // 1 hour
            });

            console.log(user);
            console.log(token);
            res.status(200).send({
                id: user._id,
                username: user.username,
                accessToken: token
            });
        });
};
